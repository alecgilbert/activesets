#ifndef ACTIVESET_H_
#define ACTIVESET_H_

#include<float.h>
#include<stdio.h>
#include<stdlib.h>
#include<tgmath.h>

#ifndef MY_FLOAT_
#define MY_FLOAT_

typedef long double my_float;

#endif

#define MAX 1000000
#define MAX_INTERVAL 25
#define PREC 3
#define SUPD 10
#define TRUE 1
#define FALSE 0

struct set {
    int *u;
    int ell;
    my_float gamma;
    struct set *next;
};

// Global variable to count the number of sets in the active set.
int numsets;

my_float A_s(int s, my_float pstar, my_float a, my_float c, my_float t);
my_float best_A(my_float eps, my_float pstar, my_float a, my_float c, my_float t);
my_float gamma_bar_pw(int *u, int ell, my_float pstar, my_float a, my_float c);
int increment_u(int *u, int ell, int i);
void quasiopt_construct(struct set *U[SUPD], my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c);
void quasiopt_construct_wrong(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c);
void opt_construct(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c);
void opt_construct_efficient_wrong(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c);
void opt_construct_efficient(struct set *U[SUPD], my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c);
int PW_construct(struct set *U[SUPD], my_float g[], my_float G[], my_float tol, long int numsets[SUPD], my_float pstar, my_float a, my_float c);
void PW_construct_naive(struct set *U[SUPD], my_float eps, my_float tol, my_float pstar, my_float a, my_float c);
my_float PW_threshold(my_float eps, my_float pstar, my_float a, my_float c, int M);
int truncationdimension(int u[], int ell, int L, my_float G[MAX], my_float g[MAX], my_float tol);
struct set *appendset(struct set **head, int *u, int ell, my_float gamma);
struct set *popset(struct set **u);
int findset(struct set *head, int *u, int ell);
int TestSetsEqual(int *u, int *v, int ell);
void addset(int *u, int ell, my_float gamma);
int deleteset(struct set **head, int *u, int ell);
void FreeList(struct set **head);
void printset(int ell, int u[]);
int set_compare(const void *x, const void *y);
// Returns the maximum of a and b as an integer.
int IntMax(int a, int b);

#endif
