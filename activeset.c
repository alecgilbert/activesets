#include"activeset.h"

// Approximates the sum:
// \sum_{|\setu| < \infty} \gamma_\setu^t
my_float A_s(int s, my_float pstar, my_float a, my_float c, my_float t) {
    my_float A = exp(pow(pow(c, pstar)/(pstar + 1), t)/((a*pstar*t - 1)*pow(s + (my_float) 1/2, a*pstar*t - 1)));
    my_float Apre;
    int j;
    
    for (j = 0; j < s; j++) {
        Apre = A;
        A *= (1 + pow(pow(c*pow(j + 1, -a), pstar)/(pstar + 1), t));
//        if (A - Apre == (my_float) 0) {
//            printf("A - Apre = %e at s = %d\n", A - Apre, j);
//        }
    }
//    printf("s = %d A = %1.16Lf\n\n", s, A);
    return A;
}

// Approximates the sum A so that the relative error is of the order of machine precision.
//
my_float best_A(my_float eps, my_float pstar, my_float a, my_float c, my_float t) {
    // Choosing best s.
//    int s = 100000000;//ceil(pow(pow(c, 2*pstar)/DBL_EPSILON, 1/(2*a*pstar - 1)));
    int s = 10000000;
    return A_s(s, pstar, a, c, t);
}

//
my_float gamma_bar_pw(int *u, int ell, my_float pstar, my_float a, my_float c) {
    my_float g;
    int j;
    
    // Case p = 1.
    if (pstar == (my_float) 0) {
        g = pow(c, ell);
        pstar = (my_float) 1;
    } else {
        g = pow(pow(c, pstar)/(pstar + 1), ell);
    }
    
    for (j = 0; j < ell; j++) g *= pow(u[j] + 1, -a*pstar);
    return g;
}

// Increments the set u from index i.
// Returns the index it incremented from.
int increment_u(int *u, int ell, int i) {
    int k;
    
    // If the increment will go beyond our tolerance we move the next index.
    while (i >= 0 && u[i] + ell - i >= MAX) {
        fprintf(stderr, "Warning: MAX dimension reached!\n");
        i--;
    }
    
    // Testing for valid index.
    if (i < 0) return i;
        
    // incrementing
    u[i]++;
    for (k = i + 1; k < ell; k++) u[k] = u[i] + k - i ;
    return i;
}

// Constructs the quasi-optimal active set.
// A is the estimate of the sum.
// interval holds the left (ie. the smallest) limit of each interval.
void quasiopt_construct(struct set *U[SUPD], my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c) {
    int i, j, k, ell, ell_next;
    int u[SUPD] = {};
    my_float g_u, T = A - pow(eps, pstar);
    struct set *L[SUPD] = {NULL}, *L_next[SUPD] = {NULL}, *current = NULL, *temp = NULL;
    
    // First subracting the the empty set
    T -= (my_float) 1;
//    printf("{} T = %Lf\n", T);
    numsets = 1;
    if (T <= 0) return;
    
    for (k = 0; k < SUPD; k++) {
        L[k] = NULL;
        L_next[k] = NULL;
    }
    
    for (j = 0; j < MAX_INTERVAL; j++) {
        ell_next = 1;
        // For each interval we add sets in increasing cardinality.
        for (ell = 1; ell <= SUPD; ell++) {
            
            // First deal with sets found at previous interval.
            for (current = popset(&(L[ell - 1])); current != NULL; current = popset(&(L[ell - 1]))) {
                // Next cardinality to be incremented from once L_j is empty.
                ell_next = ell + 1;
                // i keeps track of where to increment the set from.
                i = ell - 1;
                // Initialising the set (vector) u that we increment from.
                for (k = 0; k < ell; k++) u[k] = current->u[k];
                g_u = current->gamma;
                // Now freeing current.
                temp = current;
                free(temp->u);
                free(temp);
                
                // Searching from the current set.
                while (i >= 0) {
                    // Cut down double handling of sets by removing any future occurrence of u.
                    deleteset(&(L[ell - 1]), u, ell);
                    if (g_u >= interval[j]) {
                        // If the current set is already in U then we know we don't need to search anymore.
                        if (findset(U[ell - 1], u, ell)) break;
                        T -= g_u;
                        appendset(&(U[ell - 1]), u, ell, g_u);
                        numsets++;
                        if (T <= 0) {
                            printf("T = %Le\n", T);
                            return;
                        }
                        i = ell - 1;
                    } else {
                        appendset(&(L_next[ell - 1]), u, ell, g_u);
                        i--;
                    }
                    // Updating the set u and the weight.
                    if (i >= 0) {
                        i = increment_u(u, ell, i);
                        g_u = gamma_bar_pw(u, ell, pstar, a, c);
                    }
                }
            }
        }
        
        // Restart search at next cardinality.
        for(ell = ell_next; ell <= SUPD; ell++) {
            // initialising the first set to be checked.
            for (k = 0; k < ell; k++) u[k] = k;
            i = ell - 1;
            g_u = gamma_bar_pw(u, ell, pstar, a, c);
            // Move on to next interval.
            if (g_u < interval[j] && ell >= c) break;

            // Working through all sets, i keeps track of where to increment from.
            while (i >= 0) {
                if (g_u >= interval[j]) {
                    // If gamma_u is in the current interval we add it to the active set
                    // and update T. Also, we can increment from the final index.
                    T -= g_u;
                    appendset(&(U[ell - 1]), u, ell, g_u);
                    numsets++;
                    if (T <= 0) {
                        printf("T = %Le\n", T);
                        return;
                    }
                    i = ell - 1;
                } else {
                    // otherwise we add it to the list of sets to check for the next interval
                    // and decrement i.
                    appendset(&(L_next[ell - 1]), u, ell, g_u);
                    i--;
                }
                // Updating the set u and the weight.
                if (i >= 0) {
                    i = increment_u(u, ell, i);
                    g_u = gamma_bar_pw(u, ell, pstar, a, c);
                }
            }
        }
        // Updating the lists L and L_next
        for (k = 0; k < SUPD; k++) {
            L[k] = L_next[k];
            L_next[k] = NULL;
        }
    }
    printf("T = %Le\n", T);
    return;
}

// Constructs the quasi-optimal active set.
// A is the estimate of the sum.
// interval holds the left (ie. the smallest) limit of each interval.
void quasiopt_construct_wrong(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c) {
    int i, j, k, ell;
    int u[SUPD] = {};
    my_float g_u, T = A - pow(eps, pstar);
    struct set *L = NULL, *L_next = NULL, *current = NULL;
    
    // First subracting the the empty set
    T -= (my_float) 1;
    printf("{} T = %Lf\n", T);
    numsets = 1;
    if (T <= 0) return;
    
    for (j = 0; j < MAX_INTERVAL; j++) {
        // For each interval we add sets in increasing cardinality.
        for (ell = 1; ell <= SUPD; ell ++) {
            // First check if we have stored any sets from the previous interval.
            if (L == NULL) {
                // initialising the first set to be checked.
                for (k = 0; k < ell; k++) u[k] = k;
                i = ell - 1;
                g_u = gamma_bar_pw(u, ell, pstar, a, c);
                if (g_u < interval[j] && ell >= c) {
                    // Move on to next interval.
                    L = L_next;
                    L_next = NULL;
                    break;
                }
                // Working through all sets, i keeps track of where to increment from.
                while (i >= 0) {
                    if (g_u >= interval[j]) {
                        // If gamma_u is in the current interval we add it to the active set
                        // and update T. Also, we can increment from the final index.
                        T -= g_u;
                        printf("T = %Lf   ", T);
                        addset(u, ell, g_u);
                        if (T <= 0) return;
                        i = ell - 1;
                    } else {
                        // otherwise we add it to the list of sets to check for the next interval
                        // and decrement i.
                        current = appendset(&L_next, u, ell, g_u);
                        i--;
                    }
                    // Updating the set u and the weight.
                    if (i >= 0) {
                        u[i]++;
                        for (k = i + 1; k < ell; k++) u[k] = u[i] + k - i ;
                        if (u[ell - 1] > MAX) break;
                        g_u = gamma_bar_pw(u, ell, pstar, a, c);
                    }
                }
            } else {
                // We look at the sets stored from the previous interval.
                current = popset(&L);
                ell = current->ell;
                for (k = 0; k < ell; k++) u[k] = current->u[k];
                g_u = current->gamma;
                while (g_u >= interval[j]) {
                    // Adding the set, updating T then incrementing u.
                    T -= g_u;
                    printf("T = %Lf   ", T);
                    addset(u, ell, g_u);
                    if (T <= 0) return;
                    // In this step we only need to increment u from the last index.
                    // All other increments will already be in L from the previous set.
                    u[ell - 1]++;
                    if (u[ell - 1] > MAX) break;
                    g_u = gamma_bar_pw(u, ell, pstar, a, c);
                }
                current = appendset(&L_next, u, ell, g_u);
            }
        }
    }
    return;
}

// Constructs an optimal active set.
void opt_construct_efficient(struct set *U[SUPD], my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c) {
    int i, j, k, ell, ell_next, num = 0;
    int u[SUPD] = {};
    my_float g_u, T = A - pow(eps, pstar), T_j;
    struct set *L[SUPD] = {NULL}, *L_unsrt = NULL, *L_next[SUPD] = {NULL};
    struct set *current = NULL, *temp = NULL, **ptrs = NULL;
    
    // First subracting the the empty set
    T -= (my_float) 1;
//    printf("{} T = %Lf\n", T);
    numsets = 1;
    if (T <= 0) return;
    
    for (k = 0; k < SUPD; k++) {
        L[k] = NULL;
        L_next[k] = NULL;
    }
    
    for (j = 0; j < MAX_INTERVAL; j++) {
        T_j = (my_float) 0;
        ell_next = 1;
        // Counting how many sets in the current interval.
        num = 0;
        
        // For each interval we add sets in increasing cardinality.
        for (ell = 1; ell <= SUPD; ell ++) {
            // First we handle the sets form the previous interval.
            for (current = popset(&(L[ell - 1])); current != NULL; current = popset(&(L[ell - 1]))) {
                // Next cardinality to be searched from.
                ell_next = ell + 1;
                // i keeps track of where to increment the set from.
                i = ell - 1;
                // Initialisng set (vector) u that we increment from.
                for (k = 0; k < ell; k++) u[k] = current->u[k];
                g_u = current->gamma;
                
                // Now freeing current.
                temp = current;
                free(temp->u);
                free(temp);
                
                while (i >= 0) {
                    // Cut down double handling of sets by removing any future occurrence of u.
                    deleteset(&(L[ell - 1]), u, ell);
                    if (g_u >= interval[j]) {
                        // If the current set is already in U then we know we don't need to search anymore.
                        if (findset(L_unsrt, u, ell) || findset(U[ell - 1], u, ell)) break;
                        i = ell - 1;
                        // Adding the set to the unsorted list then incrementing u.
                        num++;
                        appendset(&L_unsrt, u, ell, g_u);
                        T_j += g_u;
                    } else {
                        appendset(&(L_next[ell - 1]), u, ell, g_u);
                        i--;
                    }
                    i = increment_u(u, ell, i);
                    g_u = gamma_bar_pw(u, ell, pstar, a, c);
                }
            }
        }
        
        for (ell = ell_next; ell <= SUPD; ell++) {
            // initialising the first set to be checked.
            for (k = 0; k < ell; k++) u[k] = k;
            i = ell - 1;
            g_u = gamma_bar_pw(u, ell, pstar, a, c);
            if (g_u < interval[j] && ell >= c) {
                // Move on to next interval.
                break;
            }
            // Working through all sets, i keeps track of where to increment from.
            while (i >= 0) {
                if (g_u >= interval[j]) {
                    // If gamma_u is in the current interval we add it to the unsorted
                    // list. Also, we can increment from the final index.
                    num++;
                    appendset(&L_unsrt, u, ell, g_u);
                    T_j += g_u;
                    i = ell - 1;
                } else {
                    // otherwise we add it to the list of sets to check for the next interval
                    // and decrement i.
                    appendset(&(L_next[ell - 1]), u, ell, g_u);
                    i--;
                }
                // Updating the set u and the weight.
                i = increment_u(u, ell, i);
                g_u = gamma_bar_pw(u, ell, pstar, a, c);
            }
        }
        
        // Now all of the sets with weights in I_j have been added to the list.
        // Check if I_j is the final interval.
        if (T_j >= T) {
            // Sort all of the sets with weights in the current interval.
            ptrs = malloc(num*sizeof(struct set *));
            for (current = L_unsrt, k = 0; current != NULL; current = current->next, k++) {
                ptrs[k] = current;
            }
            qsort(ptrs, num, sizeof(struct set *), set_compare);
            // Adding sorted sets until tolerance is less than zero.
            for (k = 0; k < num; k++) {
                T -= ptrs[k]->gamma;
                appendset(&(U[ptrs[k]->ell- 1]), ptrs[k]->u, ptrs[k]->ell, ptrs[k]->gamma);
                numsets++;
                if (T <= 0) {
                    printf("T = %Le\n", T);
                    return;
                }
            }
            free(ptrs);
            ptrs = NULL;
        } else {
            // Otherwise update T and add all sets ignoring order.
            T -= T_j;
            for (current = L_unsrt; current != NULL; current = current->next) {
                appendset(&(U[current->ell -1]), current->u, current->ell, current->gamma);
                numsets++;
            }
        }
        // Freeing unsorted list.
        while (L_unsrt != NULL) {
            current = L_unsrt;
            L_unsrt = L_unsrt->next;
            free(current->u);
            free(current);
        }
        current = NULL;
        // Updating the lists L and L_next
        for (k = 0; k < SUPD; k++) {
           L[k] = L_next[k];
           L_next[k] = NULL;
        }
    }
    printf("T = %Le\n", T);
    return;
}

// Constructs an optimal active set.
// Only sorts sets from final interval.
void opt_construct(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c) {
    int i, j, k, ell, num = 0;
    int u[SUPD] = {};
    my_float g_u, T = A - pow(eps, pstar), T_j;
    struct set *L = NULL, *L_unsrt = NULL, *L_next = NULL, *current = NULL, **ptrs = NULL;
    
    // First subracting the the empty set
    T -= (my_float) 1;
    printf("{} T = %Lf\n", T);
    numsets = 1;
    if (T <= 0) return;
    
    for (j = 0; j < MAX_INTERVAL; j++) {
        T_j = (my_float) 0;
        // Counting how many sets in the current interval.
        num = 0;
        // For each interval we add sets in increasing cardinality.
        for (ell = 1; ell <= SUPD; ell ++) {
            printf("1: ell = %d\n", ell);
            // First check if we have stored any sets from the previous interval.
            if (L == NULL) {
                // initialising the first set to be checked.
                for (k = 0; k < ell; k++) u[k] = k;
                i = ell - 1;
                g_u = gamma_bar_pw(u, ell, pstar, a, c);
                if (g_u < interval[j] && ell >= c) {
                    // Move on to next interval.
                    L = L_next;
                    L_next = NULL;
                    break;
                }
                // Working through all sets, i keeps track of where to increment from.
                while (i >= 0) {
                    if (g_u >= interval[j]) {
                        // If gamma_u is in the current interval we add it to the unsorted
                        // list. Also, we can increment from the final index.
                        num++;
                        appendset(&L_unsrt, u, ell, g_u);
                        T_j += g_u;
                        i = ell - 1;
                    } else {
                        // otherwise we add it to the list of sets to check for the next interval
                        // and decrement i.
                        current = appendset(&L_next, u, ell, g_u);
                        i--;
                    }
                    // Updating the set u and the weight.
                    if (i >= 0) {
                        u[i]++;
                        for (k = i + 1; k < ell; k++) u[k] = u[i] + k - i ;
                        g_u = gamma_bar_pw(u, ell, pstar, a, c);
                    }
                }
            }
        }
        // Now we sort all of the sets with weights in the current interval.
        ptrs = malloc(num*sizeof(struct set *));
        for (current = L_unsrt, k = 0; current != NULL; current = current->next, k++) {
            ptrs[k] = current;
        }
        qsort(ptrs, num, sizeof(struct set *), set_compare);
        // Adding sorted sets until tolerance is less that zero.
        for (k = 0; k < num; k++) {
            T -= ptrs[k]->gamma;
            printset(ptrs[k]->ell, ptrs[k]->u);
            printf(" gamma = %1.2Le T = %Lf\n", ptrs[k]->gamma, T);
            if (T <= 0) return;
        }
        // Freeing unsorted list.
        while (L_unsrt != NULL) {
            current = L_unsrt;
            L_unsrt = L_unsrt->next;
            free(current->u);
            free(current);
        }
        current = NULL;
        free(ptrs);
        ptrs = NULL;

    }
    return;
}

// Constructs an optimal active set.
// Only sorts sets from final interval.
void opt_construct_efficient_wrong(my_float A, my_float eps, my_float interval[MAX_INTERVAL], my_float pstar, my_float a, my_float c) {
    int i, j, k, ell, num = 0;
    int u[SUPD] = {};
    my_float g_u, T = A - pow(eps, pstar), T_j;
    struct set *L = NULL, *L_unsrt = NULL, *L_next = NULL, *current = NULL, **ptrs = NULL;
    
    // First subracting the the empty set
    T -= (my_float) 1;
    printf("{} T = %Lf\n", T);
    numsets = 1;
    if (T <= 0) return;
    
    for (j = 0; j < MAX_INTERVAL; j++) {
        T_j = (my_float) 0;
        // Counting how many sets in the current interval.
        num = 0;
        // For each interval we add sets in increasing cardinality.
        for (ell = 1; ell <= SUPD; ell ++) {
            // First check if we have stored any sets from the previous interval.
            if (L == NULL) {
                // initialising the first set to be checked.
                for (k = 0; k < ell; k++) u[k] = k;
                i = ell - 1;
                g_u = gamma_bar_pw(u, ell, pstar, a, c);
                if (g_u < interval[j] && ell >= c) {
                    // Move on to next interval.
                    L = L_next;
                    L_next = NULL;
                    break;
                }
                // Working through all sets, i keeps track of where to increment from.
                while (i >= 0) {
                    if (g_u >= interval[j]) {
                        // If gamma_u is in the current interval we add it to the unsorted
                        // list. Also, we can increment from the final index.
                        num++;
                        appendset(&L_unsrt, u, ell, g_u);
                        T_j += g_u;
                        i = ell - 1;
                    } else {
                        // otherwise we add it to the list of sets to check for the next interval
                        // and decrement i.
                        current = appendset(&L_next, u, ell, g_u);
                        i--;
                    }
                    // Updating the set u and the weight.
                    if (i >= 0) {
                        u[i]++;
                        for (k = i + 1; k < ell; k++) u[k] = u[i] + k - i ;
                        g_u = gamma_bar_pw(u, ell, pstar, a, c);
                    }
                }
            } else {
                // We look at the sets stored from the previous interval.
                current = popset(&L);
                ell = current->ell;
                for (k = 0; k < ell; k++) u[k] = current->u[k];
                g_u = current->gamma;
                while (g_u >= interval[j]) {
                    // Adding the set to the unsorted list then incrementing u.
                    num++;
                    appendset(&L_unsrt, u, ell, g_u);
                    T_j += g_u;
                    // In this step we only need to increment u from the last index.
                    // All other increments will already be in L from the previous interval.
                    u[ell - 1]++;
                    g_u = gamma_bar_pw(u, ell, pstar, a, c);
                }
                current = appendset(&L_next, u, ell, g_u);
            }
        }
        // Now all of the sets with weights in I_j have been added to the list.
        // Check if I_j is the final interval.
        if (T_j >= T) {
            // Sort all of the sets with weights in the current interval.
            ptrs = malloc(num*sizeof(struct set *));
            for (current = L_unsrt, k = 0; current != NULL; current = current->next, k++) {
                ptrs[k] = current;
            }
            qsort(ptrs, num, sizeof(struct set *), set_compare);
            // Adding sorted sets until tolerance is less than zero.
            for (k = 0; k < num; k++) {
                T -= ptrs[k]->gamma;
                printf("T = %Lf   ", T);
                addset(ptrs[k]->u, ptrs[k]->ell, ptrs[k]->gamma);
                if (T <= 0) return;
            }
            free(ptrs);
            ptrs = NULL;
        } else {
            // Otherwise update T and add all sets ignoring order.
            T -= T_j;
            for (current = L_unsrt; current != NULL; current = current->next) {
                printf("T = %Lf   ", T);
                addset(current->u, current->ell, current->gamma);
            }
        }
        // Freeing unsorted list.
        while (L_unsrt != NULL) {
            current = L_unsrt;
            L_unsrt = L_unsrt->next;
            free(current->u);
            free(current);
        }
        current = NULL;
    }
    return;
}

void PW_construct_naive(struct set *U[SUPD], my_float eps, my_float tol, my_float pstar, my_float a, my_float c) {
    int i, k, ell;
    int u[SUPD] = {};
    my_float g_u;
    // First adding the the empty set
    numsets = 1;
    
    // Add sets in increasing cardinality.
    for (ell = 1; ell <= SUPD; ell ++) {
        // initialising the first set to be checked.
        for (k = 0; k < ell; k++) u[k] = k;
        i = ell - 1;
        g_u = gamma_bar_pw(u, ell, pstar, a, c);

        // Working through all sets, i keeps track of where to increment from.
        while (i >= 0) {
            if (g_u >= tol) {
                // If gamma_u is greater than the tolerance we add it to U
                // Also, we can increment from the final index.
//                addset(u, ell, g_u);
                appendset(&(U[ell - 1]), u, ell, g_u);
                numsets++;
                printf("|U^PW| = %d\n", numsets);
                i = ell - 1;
            } else {
                // Decrement index.
                i--;
            }
            // Updating the set u and the weight.
            if (i >= 0) {
                u[i]++;
                for (k = i + 1; k < ell; k++) u[k] = u[i] + k - i ;
                g_u = gamma_bar_pw(u, ell, pstar, a, c);
            }
        }
    }
    return;
}

my_float PW_threshold(my_float eps, my_float pstar, my_float a, my_float c, int M) {
    int j;
    my_float T = 0, t;
    if (pstar == (my_float) 0) {
        return eps;
    }
    
    eps = pow(eps, pstar);
    
    for (j = ceil(M/(a*pstar)); j < M; j++) {
        t = (my_float) j/M;
        T = fmax(T, pow(best_A(eps, pstar, a, c, t)/eps, -1/(1 - t)));
    }
    return T;
}

// Appends the set u to the list starting at head.
// Returns a pointer to the element just added.
struct set* appendset(struct set **head, int *u, int ell, my_float gamma) {
    int j;
    struct set *current = NULL, *temp = NULL;
    
    // initialising the new set,
    temp = (struct set *) malloc(sizeof(struct set));
    if (temp == NULL) {
        fprintf(stderr, "Error: unable to allocate memory at line %d.\n", __LINE__ - 2);
        exit(-1);
    }
    temp->u = malloc(ell*sizeof(int));
    for (j = 0; j < ell; j++) temp->u[j] = u[j];
    temp->ell = ell;
    temp->gamma = gamma;
    temp->next = NULL;
    // Appending the set to the list.
    // First creating list.
    if (*head == NULL) {
        *head = temp;
    } else {
        for (current = *head; current->next != NULL; current = current->next);
        current->next = temp;
    }

    return temp;
}

// Pops the first element from the list.
// Returns the popped set and updates the head of the list to point to the next element.
// If the list is empty it simply returns NULL.
struct set *popset(struct set **head) {
    struct set *tmp = *head;
    
    if (*head) *head = (*head)->next;

    return tmp;
}

// Determines whether a given set u is in the list starting at head.
int findset(struct set *head, int *u, int ell) {
    int found =  0;
    struct set *current = NULL;
    
    for (current = head; current != NULL && !found; current = current->next) {
        found = TestSetsEqual(u, current->u, ell);
    }
    return found;
}

// Determines whether two sets u and v are identical.
int TestSetsEqual(int *u, int *v, int ell) {
    int k, same = 1;
    for (k = 0; k < ell; k++) {
        if (u[k] != v[k]) {
            same = 0;
            return same;
        }
    }
    return same;
}
// Deletes a given set from the list.
// Returns 1 if the set was found and 0 otherwise.
int deleteset(struct set **head, int *u, int ell) {
    struct set *current = *head, *pre = NULL;
    
    // Check that the list is non-empty.
    if (!*head) return 0;
    
    // First test the first set.
    if (TestSetsEqual(u, (*head)->u, ell)) {
        *head = (*head)->next;
        free(current->u);
        free(current);
        return 1;
    }
    
    for (current = (*head)->next; current != NULL; current = current->next) {
        if (TestSetsEqual(u, current->u, ell)) {
            // Rearrange pointers and free set.
            pre->next = current->next;
            free(current->u);
            free(current);
            return 1;
        }
        pre = current;
    }
    return 0;
}

void FreeList(struct set **head) {
    struct set *current;
    
    // If the list is already empty we simply return.
    if (!*head) return;
    
    for (current = *head, *head = (*head)->next; *head != NULL; *head = (*head)->next) {
        free(current->u);
        free(current);
        current = *head;
    }
    free(current->u);
    free(current);
    return;
}
// Prints the set S of indices, |S|.
void printset(int ell, int u[]) {
    int j;
    
    printf("{%d", u[0] + 1);
    for (j = 1; j < ell; j++) printf(", %d", u[j] + 1);
    printf("}");
}

// Compares the weight of two sets, takes two pointers as input.
// Will sort in decreasing order.
int set_compare(const void *x, const void *y) {
    struct set *u = *(struct set * const *) x;
    struct set *v = *(struct set * const *) y;
    my_float a = u->gamma;
    my_float b = v->gamma;
    
    if (a < b) {
        return 1;
    } else if (a > b) {
        return -1;
    } else {
        return 0;
    }
}

// Constructing the active set for POD weights. The active set is an array linked lists of sets.
int PW_construct(struct set *U[SUPD], my_float g[], my_float G[], my_float tol, long int numsets[SUPD], my_float pstar, my_float a, my_float c) {
    struct set *current = NULL;// = *U;
    int D, Du;
    int L = 0;
    int d, ell;
    int i, j;
    int found, max_index;
    // my_float sum = 0;
    int *u = NULL;
    
    for (ell = 0; ell < SUPD; ell++) {
        //        current = U[l];
        
        // Array to count the number of sets of a given cardinality.
        numsets[ell] = 0;
        // Allocating memory for the set u.
        u = malloc((ell + 1)*sizeof(int));
        if (u == NULL) {
            fprintf(stderr, "Out of memory: Line %d\n", __LINE__ - 2);
            exit(-1);
        }
        
        // Initialising the set u = {1, 2, ..., l-1}
        for (j = 0; j < ell; j++) u[j] = j;
        
        
        // Calculating the truncation dimension for the current cardinality.
        D = truncationdimension(u, ell - 1, MAX, G, g, tol);
        
        // If the current truncation dimension is 0 then U is complete.
        if (D == 0) {
            L = ell;
            break;
        } else if (D >= MAX - 1) {
            printf("Warning: Max truncation dimension reached for sets of size %d\n", ell + 1);
        }
        
        // Adding the sets (u, d) to U for d up to the truncation dimension.
        for (d = ell; d <= D; d++) {
            u[ell] = d;
            addset(u, ell, gamma_bar_pw(u, ell, pstar, a, c));
//            if (U[ell] == NULL) U[ell] = current;
        }
        
        // Increment the intermediary elements of u.
        for (max_index = D; max_index >= 0; ) {
            found = FALSE;
            // Identifying max index.
            for (i = ell - 1; i >= 0; i--) {
                if (u[i] < D - ell + i  && i <= max_index) {
                    found = TRUE;
                    break;
                }
            }
            // If no such index exists then move on to the sets of higher order.
            if (!found) break;
            
            // Starting at i, incrementing the subsequent indices by 1.
            u[i]++;
            for (j = i + 1; j < ell; j++) u[j] = u[i] + j - i;
            
            Du = truncationdimension(u, ell - 1, D, G, g, tol);
            // If no more increments are possible decrement the max index.
            if (Du == 0) max_index = i;
            
            // Adding (u, d) to U for d <= Du.
            for (d = u[ell-1] + 1; d <= Du; d++) {
                u[ell] = d;
                addset(u, ell, gamma_bar_pw(u, ell, pstar, a, c));
            }
        }
        free(u);
        u = NULL;
        current = NULL;
    }
    return L;
}

// Calculates the maximum truncation dimension of the set u where the first ell - 1 elements
// are given.
int truncationdimension(int u[], int ell, int L, my_float G[MAX], my_float g[MAX], my_float tol) {
    int d;
    int maxu;
    
    // maxu is the maximum element of u. It is the starting value for the
    // truncation dimension. l = -1 corresponds to the empty set.
    if (ell == -1) {
        maxu = 0;
    } else {
        maxu = u[ell];
    }
    
    if (L > MAX) {
        fprintf(stderr, "Warning: L > MAX at line %d\n", __LINE__);
        return 0;
    }
    
    tol /= G[ell + 1];
    for (d = 0; d <= ell; d++) tol /= g[u[d]]*G[d];
    
    // Checking that the first element is greater than the tolerance otherwise set
    // the truncation dimension to 0. This is equivalent to the set of indices for
    // which the truncation dimension is defined being non-empty.
    if (g[u[IntMax(ell, 0)] + 1] <= tol) {
        return 0;
    }
    
    for (d = maxu + 1; g[maxu + 1] > tol && d < L; d++) {
        // Truncation dimension found, return previous index.
        if (g[d] <= tol) return d - 1;
    }
    // If no such index exists the max is returned.
    return d - 1;
}

void addset(int *u, int ell, my_float gamma) {
    printset(ell, u);
    printf("   gamma = %Le\n", gamma);
    return;
}

// Returns the maximum of a and b as an integer.
int IntMax(int a, int b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}
