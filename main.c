#include<stdlib.h>
#include<stdio.h>
#include<tgmath.h>
#include"activeset.h"

int main(int argc, char *argv[]) {
    // Parameters
    my_float p = (my_float) 2, pstar = (my_float) 1, a = (my_float)  2, c = (my_float) 1;
    my_float eps = (my_float) 1, tol;
    int j, k, ell, supdim = 0;
    my_float A;
    my_float interval[MAX_INTERVAL];
    struct set *U[SUPD] = {NULL}, *current = NULL;
    
//    for (c = (my_float) 1/2; c < 3; c *= (my_float) 2) {
//        for (a = 4; a > 1; a--) {
            printf("p^* = %.1Lf   a = %.1Lf   c = %.1Lf\n", pstar, a, c);
            A = best_A(eps, pstar, a, c, (my_float) 1);
        
            printf("A = %Lf\n", A);

        printf("quasi-optimal sets:\n");
        for (k = 1; k <= PREC; k++) {
            eps = pow((my_float) 10, (my_float) -k);
            printf("+++++++ eps = %1.0Le +++++++\n\n", eps);
            for (j = 0; j < MAX_INTERVAL; j++) interval[j] = pow(10, -(j + 1));
            quasiopt_construct(U, A, eps, interval, pstar, a, c);
            printf("|U^q-opt| = %d\n", numsets);
            for (ell = 0; ell < SUPD; ell++) {
                if (U[ell]) {
                    supdim = ell + 1;
//                    for (current = U[ell]; current != NULL; current = current->next) {
//                        printset(ell + 1, current->u);
//                        printf("\n");
//                    }
                }
                FreeList(&(U[ell]));
            }
            printf("Superposition dimension = %d\n\n", supdim);
            supdim = 0;
        }
        
            printf("optimal sets (efficient):\n");
            for (k = PREC; k <= PREC; k++) {
                eps = pow((my_float) 10, (my_float) -k);
                printf("+++++++ eps = %1.0Le +++++++\n\n", eps);
                for (j = 0; j < MAX_INTERVAL; j++) interval[j] = pow(10, -(j + 1));
                opt_construct_efficient(U, A, eps, interval, pstar, a, c);
                printf("|U^opt| = %d\n", numsets);
                for (ell = 0; ell < SUPD; ell++) {
                    if (U[ell]) {
                        supdim = ell + 1;
//                        for (current = U[ell]; current != NULL; current = current->next) {
//                            printset(ell + 1, current->u);
//                            printf("\n");
//                        }
                    }
                    FreeList(&(U[ell]));
                }
                printf("Superposition dimension = %d\n\n", supdim);
                supdim = 0;
            }

        printf("PW sets:\n");
        for (k = PREC; k <= PREC; k++) {
            eps = pow((my_float) 10, (my_float) -k);
            printf("+++++++ eps = %1.0Le +++++++\n\n", eps);
            for (j = 0; j < MAX_INTERVAL; j++) interval[j] = pow(10, -(j + 1));
            tol = PW_threshold(eps, pstar, a, c, 40);
            printf("tol = %Le\n", tol);
            PW_construct_naive(U, eps, tol, pstar, a, c);
            printf("|U^PW| = %d\n", numsets);
            for (ell = 0; ell < SUPD; ell++) {
                if (U[ell]) supdim = ell + 1;
                FreeList(&(U[ell]));
            }
            printf("Superposition dimension = %d\n\n", supdim);
            supdim = 0;
//        }
//        }
    }
    return 0;
}
