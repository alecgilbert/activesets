CC = gcc
CFLAGS = -Wall -std=c99
LIBS = -lgsl -lgslcblas -lm
DEPS = activeset.h

main : main.c activeset.o
	$(CC) -o main main.c activeset.c $(CFLAGS) $(LIBS)

activeset.o : activeset.c activeset.h
	$(CC) -c -o $@ $< $(CFLAGS)

clean :
	rm main activeset.o
